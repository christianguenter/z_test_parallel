*&---------------------------------------------------------------------*
*& Report z_test_parallel
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_test_parallel.

CLASS controller DEFINITION.

  PUBLIC SECTION.
    METHODS: start.

ENDCLASS.

CLASS lcl_parallel DEFINITION
                   INHERITING FROM cl_abap_parallel.

  PUBLIC SECTION.
    METHODS:
      do REDEFINITION.

ENDCLASS.

CLASS controller IMPLEMENTATION.

  METHOD start.

    DATA: in_tab  TYPE cl_abap_parallel=>t_in_tab,
          in      LIKE LINE OF in_tab,
          out_tab TYPE cl_abap_parallel=>t_out_tab,
          ls_t100 TYPE t100.

    SELECT FROM t100
           FIELDS *
           INTO TABLE @DATA(lt_t100)
           UP TO 100 ROWS.

    LOOP AT lt_t100 ASSIGNING FIELD-SYMBOL(<ls_t100>).
      EXPORT t100 = <ls_t100> TO DATA BUFFER in.
      INSERT in INTO TABLE in_tab.
    ENDLOOP.

    CLEAR: lt_t100.

    NEW lcl_parallel( )->run(
      EXPORTING
        p_in_tab  = in_tab
      IMPORTING
        p_out_tab = out_tab ).

    LOOP AT out_tab ASSIGNING FIELD-SYMBOL(<out>).
      IMPORT t100 = ls_t100 FROM DATA BUFFER <out>-result.
      INSERT ls_t100 INTO TABLE lt_t100.
    ENDLOOP.

    cl_demo_output=>display( lt_t100 ).

  ENDMETHOD.

ENDCLASS.

CLASS lcl_parallel IMPLEMENTATION.

  METHOD do.

    DATA: ls_t100 TYPE t100.

    IMPORT t100 = ls_t100 FROM DATA BUFFER p_in.

    ls_t100-text = |1: { ls_t100-text }|.

    DO 10000000 TIMES.

      DATA(x) = 2 * 3 /  7.

    ENDDO.

    EXPORT t100 = ls_t100 TO DATA BUFFER p_out.

  ENDMETHOD.

ENDCLASS.

START-OF-SELECTION.
  NEW controller( )->start( ).
